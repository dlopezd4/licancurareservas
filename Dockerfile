FROM mcr.microsoft.com/dotnet/core/aspnet:2.1 AS runtime
WORKDIR /app
COPY LicancuraReservas/out/. ./
CMD ASPNETCORE_URLS=http://*:$PORT dotnet LicancuraReservas.dll