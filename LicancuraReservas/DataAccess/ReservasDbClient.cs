﻿using Dapper;
using LicancuraReservas.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;

namespace LicancuraReservas.DataAccess
{
    public class ReservasDbClient
    {
        private string _connStr { get; set; }

        public ReservasDbClient(IConfiguration configuration)
        {
            _connStr = configuration.GetConnectionString("reservas");
        }

        public void AgregarReserva(Reserva reserva)
        {
            using (var connection = new NpgsqlConnection(_connStr))
            {
                var queryString = "insert into dbo.reservas (titulo, inicio, fin) values ('" + reserva.Title + "', '" + string.Format("{0:yyyy-MM-dd}",reserva.Start) + "', '" + string.Format("{0:yyyy-MM-dd}", reserva.End) + "');";
                connection.Open();
                connection.Execute(queryString);
            }
        }

        public void ActualizarReserva(Reserva reserva)
        {
            using (var connection = new NpgsqlConnection(_connStr))
            {
                var queryString = "update dbo.reservas set titulo = '"+ reserva.Title + "', inicio = '"+ string.Format("{0:yyyy-MM-dd}", reserva.Start)  + "', fin = '"+ string.Format("{0:yyyy-MM-dd}", reserva.End) + "' where id = "+ reserva.Id+";";
                connection.Open();
                connection.Execute(queryString);
            }
        }

        public List<Reserva> GetReservas()
        {
            var reservas = new List<Reserva>();
            using (var connection = new NpgsqlConnection(_connStr))
            {
                var queryString = "select id, titulo, inicio, fin from dbo.reservas where inicio >= NOW() - interval '1 month' and inicio < now() + interval '3 month';";
                connection.Open();
                var reader = connection.ExecuteReader(queryString);
                while(reader.Read())
                {
                    var reserva = new Reserva();
                    reserva.Id = reader.GetInt64(0);
                    reserva.Title = reader.GetString(1);
                    reserva.Start = reader.GetDateTime(2);
                    reserva.End = reader.GetDateTime(3);

                    reservas.Add(reserva);
                }
            }

            return reservas;
        }

        public Reserva GetReserva(long id)
        {
                    var reserva = new Reserva();

            using (var connection = new NpgsqlConnection(_connStr))
            {
                var queryString = "select id, titulo, inicio, fin from dbo.reservas where id=" + id +";";
                connection.Open();
                var reader = connection.ExecuteReader(queryString);
                while (reader.Read())
                {
                    reserva.Id = reader.GetInt64(0);
                    reserva.Title = reader.GetString(1);
                    reserva.Start = reader.GetDateTime(2);
                    reserva.End = reader.GetDateTime(3);

                }
            }

            return reserva;
        }

        public void EliminarReserva(long id)
        {
            using (var connection = new NpgsqlConnection(_connStr))
            {
                var queryString = "delete from dbo.reservas where id = " + id + ";";
                connection.Open();
                connection.Execute(queryString);
            }
        }
    }
}
