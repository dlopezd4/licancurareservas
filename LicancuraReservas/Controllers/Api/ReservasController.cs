﻿using System.Collections.Generic;
using LicancuraReservas.DataAccess;
using LicancuraReservas.Models;
using Microsoft.AspNetCore.Mvc;

namespace LicancuraReservas.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReservasController : ControllerBase
    {
        private ReservasDbClient _reservasDbClient;

        public ReservasController(ReservasDbClient reservasDbClient)
        {
            _reservasDbClient = reservasDbClient;
        }


        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Reserva>> Get([FromQuery]string access)
        {
            var reservas = _reservasDbClient.GetReservas();
            if(access == "public")
                reservas.ForEach(e => e.Title = "Reservado");
            reservas.ForEach(e => e.End = e.End.AddHours(23));
            return reservas;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] Reserva reserva)
        {
            _reservasDbClient.AgregarReserva(reserva);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(long id)
        {

        }
    }
}
