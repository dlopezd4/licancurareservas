﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LicancuraReservas.DataAccess;
using LicancuraReservas.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LicancuraReservas.Controllers
{
    public class ReservasController : Controller
    {
        private readonly ReservasDbClient _reservasDbClient;

        public ReservasController(ReservasDbClient reservasDbClient)
        {
            _reservasDbClient = reservasDbClient;
        }

        // GET: Reserva
        public ActionResult Index()
        {
            var reservas = _reservasDbClient.GetReservas();
            return View(reservas);
        }

        // GET: Reserva/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Reserva/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Reserva/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([FromForm] Reserva reserva)
        {
            try
            {
                _reservasDbClient.AgregarReserva(reserva);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Reserva/Edit/5
        public ActionResult Edit(long id)
        {
            var reserva = _reservasDbClient.GetReserva(id);
            return View(reserva);
        }

        // POST: Reserva/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([FromForm] Reserva reserva)
        {
            try
            {
                _reservasDbClient.ActualizarReserva(reserva);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Reserva/Delete/5
        public ActionResult Delete(long id)
        {
            _reservasDbClient.EliminarReserva(id);
            return RedirectToAction("Index");
        }

        // POST: Reserva/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}